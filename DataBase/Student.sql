﻿CREATE TABLE [dbo].[Student]
(
	[StudentID] INT	IDENTITY (1,1)	NOT NULL , 
    [LastName] NCHAR(60) NULL, 
    [FirstName] NCHAR(60) NULL, 
	[MiddleName] NVARCHAR(60) NULL, 
    [EnrollmentDate] DATETIME NULL,
    PRIMARY KEY CLUSTERED ([StudentID] ASC)
)
