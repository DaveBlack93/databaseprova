﻿MERGE INTO Course AS Target
USING (VALUES 
			(1, 'ECONOMIC', 3),
			(2, 'LITERATURE', 3),
			(3, 'CHEMISTRY', 4)
			)
AS Source (CourseID, Title, Credits)
ON Target.CourseID = Source.CourseID
WHEN NOT MATCHED BY TARGET THEN
INSERT (Title, Credits)
VALUES (Title, Credits);

MERGE INTO Student AS Target
USING (VALUES 
			(1, 'Tibbet', 'Donnie', '2013-09-01'),
			(2, 'Gutzam', 'Lisa', '2011-08-05'),
			(3, 'Carret', 'Phil', '2012-11-09')
			)
AS Source (StudentID, LastName, FirstName, EnrollmentDate)
ON Target.StudentID = Source.StudentID
WHEN NOT MATCHED BY TARGET THEN
INSERT (LastName, FirstName, EnrollmentDate)
VALUES (LastName, FirstName, EnrollmentDate);

MERGE INTO Enrollment AS Target
USING (VALUES 
			(1, 2.32, 1, 1),
			(2, 3.51, 1, 2),
			(3, 4.56, 2, 3),
			(4, 1.88, 2, 1),
			(5, 3.21, 3, 1),
			(6, 4.06, 3, 2)
			)
AS Source (EnrollmentID, Grade, CourseID, StudentID)
ON Target.EnrollmentID = Source.EnrollmentID
WHEN NOT MATCHED BY TARGET THEN
INSERT (Grade, CourseID, StudentID)
VALUES (Grade, CourseID, StudentID);